from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import ReadGTF as R
import os
import sys
import re
import json
import matplotlib
import itertools
import copy
from time import time
from collections import Counter
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib_venn import venn3
import scipy.stats as stats
import numpy as np
from operator import itemgetter
import ReadGTF as R
import ReadSAM as RSAM

def getExonNumDistribution(GTF):
    exon_nums = []
    for chr in GTF.chrGroup:
        for tsc in GTF.chrGroup[chr].transcripts:
            exon_nums.append(len(tsc.exons))
    return exon_nums     

def getLenDistribution(GTF):
    exon_lens = []
    tsc_lens = []
    for chr in GTF.chrGroup:
        for tsc in GTF.chrGroup[chr].transcripts:
            tsc_len = 0
            for exon in tsc.exons:
                exon_len = exon.getLength()
                exon_lens.append(exon_len)
                tsc_len += exon_len
            tsc_lens.append(tsc_len)
    return exon_lens, tsc_lens

def generateData(out_dir = "json"):
    #####################################################
    #         Plot hist at isoform level                #
    #####################################################
    filename = "result/result_20151026_beta_v5/isolinks_transcripts.gtf"
    query = R.ReadGTF(filename)
    query.processGTF()
    query.calculateTPM(expression='READ_VALUE')
    query.summaryStatistics()
    
    filename = "comparison/cufflinks_short/transcripts.gtf"
    query_cuff = R.ReadGTF(filename)
    query_cuff.processGTF()
    query_cuff.calculateTPM(expression = 'FPKM')
    query_cuff.summaryStatistics()
    
    filename = "result/result_20151026_beta_v5/pacbio_transcripts.gtf"
    query_pacbio = R.ReadGTF(filename)
    query_pacbio.processGTF()
    query_pacbio.calculateTPM(expression='READ_VALUE')
    query_pacbio.summaryStatistics() 
    
    filename = "comparison/GENCODE_23/gencode.v23.chr_patch_hapl_scaff.annotation.sorted.gtf"
    anno = R.ReadGTF(filename)
    anno.processGTF(style="GENCODE")
    anno.summaryStatistics()
    label = ['Isolinks', 'Cufflilnks', 'Pacbio', 'GENCODE']
    exon_nums = [ getExonNumDistribution(query),
                  getExonNumDistribution(query_cuff),
                  getExonNumDistribution(query_pacbio),
                  getExonNumDistribution(anno) ]
    exon_tsc_lens = [ getLenDistribution(query),
                      getLenDistribution(query_cuff),
                      getLenDistribution(query_pacbio),
                      getLenDistribution(anno) ]
    exon_lens = map(lambda x: x[0], exon_tsc_lens)
    tsc_lens  = map(lambda x: x[1], exon_tsc_lens)
    
    #####################################################
    #         Plot overlap venn at isoform level        #
    #####################################################
    # Isolinks
    file1 = "result/result_20151026_beta_v5/iso_annotated.gtf"
    query_anno = R.ReadGTF(file1)
    query_anno.processGTF()
    
    file2 = "result/result_20151026_beta_v5/iso_isoforms.gtf"
    query_iso = R.ReadGTF(file2)
    query_iso.processGTF()
    
    query_anno.combineWith(query_iso)
    query_anno.summaryStatistics()
    
    # Cufflinks
    file1 = "result/result_20151026_beta_v5/cuff_annotated.gtf"
    query_cuff_anno = R.ReadGTF(file1)
    query_cuff_anno.processGTF()
    
    file2 = "result/result_20151026_beta_v5/cuff_isoforms.gtf"
    query_cuff_iso = R.ReadGTF(file2)
    query_cuff_iso.processGTF()
    
    query_cuff_anno.combineWith(query_cuff_iso)
    query_cuff_anno.summaryStatistics()
    
    # Pacbio
    file1 = "result/result_20151026_beta_v5/pacbio_annotated.gtf"
    query_pac_anno = R.ReadGTF(file1)
    query_pac_anno.processGTF()
    
    file2 = "result/result_20151026_beta_v5/pacbio_isoforms.gtf"
    query_pac_iso = R.ReadGTF(file2)
    query_pac_iso.processGTF()
    
    query_pac_anno.combineWith(query_pac_iso)
    query_pac_anno.summaryStatistics()
    
    gene_iso_num_0 = [ query_anno.getGeneIsoNum(), 
                       query_cuff_anno.getGeneIsoNum(),
                       query_pac_anno.getGeneIsoNum(),
                       anno.getGeneIsoNum()  ]
    
    gene_iso_num_2 = [ query_anno.getGeneIsoNum(thre=2), 
                       query_cuff_anno.getGeneIsoNum(thre=2),
                       query_pac_anno.getGeneIsoNum(thre=2),
                       anno.getGeneIsoNum(thre=2)  ]
    
    out_json = { 'exon_nums': exon_nums,
                 'exon_lens': exon_lens,
                 'tsc_lens' : tsc_lens,
                 'gene_iso_num_0': gene_iso_num_0,
                 'gene_iso_num_2': gene_iso_num_2
                }
    with open(os.path.join(out_dir, "plot.json"), 'w') as f:
        f.write(json.dumps(out_json, indent=2))


def normalize(data):
    total = sum(data)
    data = map(lambda x: x/total, data)
    return data
    
def plotHist(data, out = "test.png", max = float("inf"), labels = ['Isolinks', 'Cufflinks', 'PacBio', 'GENCODE'],
             x_label = 'Exon number within individual isoform', y_label="Isoform ratio", bin=50):
    plot_data = []
    for line in data:
        if isinstance(line, list):
            for i,each in enumerate(line):
                if line[i] >= max:
                    line[i] = max
        elif isinstance(line, dict):
            line_data = []
            for gene in line:
                if line[gene] >= max:
                    line[gene] = max
                line_data.append(line[gene])
            plot_data.append(line_data)
    if len(plot_data):
        data = plot_data
    plt.figure(figsize = (14,10))
    plt.hist(data, bin, normed = 1, histtype='bar', label=labels, linewidth = 0)
    plt.legend(loc = "upper right",bbox_to_anchor = (1,1), fontsize=33) 
    plt.xlabel(x_label, fontsize=35)
    plt.ylabel(y_label, fontsize= 35)
    plt.xticks(fontsize = 27)
    plt.yticks(fontsize = 27)
    plt.tight_layout()
    plt.savefig(out)
    
    
    
if __name__ == "__main__":
    generateData()
    with open('json/plot.json', 'r') as f:
        obj = json.loads(f.read())
    out_dir = "analysis"
    plotHist(obj['exon_nums'], out=os.path.join(out_dir,"hist_iso_exon.png"), max = 25, bin=25)
    plotHist(obj['exon_lens'], out=os.path.join(out_dir,"hist_exon_len.png"), max = 1000
             , x_label = 'Exon length(bp)', y_label = "Exon ratio")
    plotHist(obj['tsc_lens'],  out=os.path.join(out_dir, "hist_tsc_len.png"), max = 10000
             , x_label = 'Isoform length(bp)', y_label = "Isoform ratio")
    plotHist(obj['gene_iso_num_0'], out=os.path.join(out_dir, "hist_gene_iso_0.png"), max = 25, bin=25
            , x_label = 'Isoform num within individual gene', y_label = "Gene ratio")
    plotHist(obj['gene_iso_num_2'], out=os.path.join(out_dir, "hist_gene_iso_2.png"), max = 25, bin=25
            , x_label = 'Multi-exon isoform num within individual gene', y_label = "Gene ratio")
    
    
    
    
    
    
    
    
    
    
    
    