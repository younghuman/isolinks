from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import os
import sys
import re
import json
import matplotlib
import itertools
import math
from time import time
from collections import Counter

matplotlib.use('Agg')
import matplotlib.pyplot as plt
from operator import itemgetter

import ReadSAM as RSAM
import ReadGTF as RGTF
def configuration():
    sys.stderr.write("NOTICE: Reading configuration from config.txt\n")
    curdir = os.path.dirname(os.path.realpath(__file__))
    config = {}
    with open(curdir+"/config.txt") as f:
        for line in f:
            if re.search(r'^#', line) or re.match(r'^\s+$',line):
                continue
            line = re.sub(r'[\r\n]+','',line)
            key,value = re.split(r'\s*=\s*',line)
            config[key] = value
    RSAM._NAME_STRING = "ISOLINKS"
    if 'exon_imprecision' in config:
        RSAM._EXON_IMPRECISION = int(config['exon_imprecision'])
    else:
        sys.stderr.write("NOTICE: Default exon_imprecision = 3 is used!\n")
        RSAM._EXON_IMPRECISION = 3
        
    if 'fln_read_max_error' in config:
        RSAM._FULL_LENGTH_MAX_ERROR = float(config['fln_read_max_error'])
    else:
        sys.stderr.write("NOTICE: Default fln_read_max_error = 0.10 is used!\n")
        RSAM._FULL_LENGTH_MAX_ERROR = 0.10
        
    if 'nfl_read_max_error' in config:
        RSAM._NFLN_MAX_ERROR = float(config['nfl_read_max_error'])
    else:
        sys.stderr.write("NOTICE: Default nfl_read_max_error = 0.15 is used!\n")
        RSAM._NFLN_MAX_ERROR = 0.15
    
    if 'read_value_min' in config:
        RSAM._READ_VALUE_MINIMUM = float(config['read_value_min'])
    else:
        sys.stderr.write("NOTICE: Default read_value_min = 0.15 is used!\n")
        RSAM._READ_VALUE_MINIMUM = 1.9
        
    if 'splice_dominant_ratio_min' in config:
        RSAM._SPLICE_DOMINANT_RATIO_MIN = float(config['splice_dominant_ratio_min'])
    else:
        sys.stderr.write("NOTICE: Default splice_dominant_ratio_min = 0.6 is used!\n")
        RSAM._SPLICE_DOMINANT_RATIO_MIN = 0.6
        
    if 'splice_range_max_fold' in config:
        RSAM._SPLICE_RANGE_MAX_FOLD = float(config['splice_range_max_fold'])
    else:
        sys.stderr.write("NOTICE: Default splice_range_max_fold = 3 is used!\n")
        RSAM._SPLICE_RANGE_MAX_FOLD = 3
        
    if 'intron_min' in config:
        RSAM._INTRON_MIN = float(config['intron_min'])
    else:
        sys.stderr.write("NOTICE: Default intron_min = 50 is used!\n")
        RSAM._INTRON_MIN = 50
        
    if 'exon_min' in config:
        RSAM._EXON_MIN = float(config['exon_min'])
    else:
        sys.stderr.write("NOTICE: Default exon_min = 10 is used!\n")
        RSAM._EXON_MIN = 10
    
    return config

def countTSCNum(tscgroups):
    num = 0
    for tscgroup in tscgroups:
        num += len(tscgroup.transcripts)
    return num

def processRead(filename, nfl_filename = None, output_dir = "."):
    if not os.path.isdir(output_dir):
        os.mkdir(output_dir)
    # Full length SAM processing
    SAM = RSAM.ReadSAM(filename)
    sys.stderr.write("NOTICE: Start processing full length SAM %s\n" % filename)
    SAM.processSAM()
    sys.stderr.write("NOTICE: Finish processing full length SAM\n")
    SAM.summaryStatistics()
    # For the purpose of read value normalization 
    good_read_count = SAM.total_reads- SAM.error_reads
    
    # Non full length SAM processing
    if nfl_filename is not None:
        NFL_SAM = RSAM.ReadSAM(nfl_filename)
        sys.stderr.write("NOTICE: Start processing non full length SAM %s\n" % nfl_filename)
        NFL_SAM.processSAM()
        sys.stderr.write("NOTICE: Finish processing non full length SAM\n" )
        NFL_SAM.summaryStatistics()
    
    #Non Parallel computing
    output = []
    splice_ratios = []
    splice_ranges = []
    start = time()
    group_num = 1
    total_nfl = 0
    total_nfl_before = 0
    for chr in SAM.chrGroup:
        tscGroups = SAM.chrGroup[chr].findNonOverlappingGroup()
        sys.stdout.write("[FULL LENGTH]NOTICE: %s has %d non-overlapping groups\n" % (chr, len(tscGroups)))
        if chr in NFL_SAM.chrGroup:
            nfl_tscGroups = NFL_SAM.chrGroup[chr].findNonOverlappingGroup()
            nfl_num_before = countTSCNum(nfl_tscGroups)
            total_nfl_before += nfl_num_before
            sys.stdout.write("[NON FULL LENGTH]NOTICE: %s has %d non-overlapping groups\n" % (chr, len(nfl_tscGroups)))
        else:
            nfl_tscGroups = None
        j = 0
        # For each Full Length Group
        for tscGroup in tscGroups:
            #sys.stdout.write("[FULL LENGTH]TSC GROUP: %s %d %d\n" % (chr, tscGroup.l_boundary, tscGroup.r_boundary))
            tscGroup.fullLengthTSCFinder()
            if nfl_tscGroups is not None:
                while j < len(nfl_tscGroups) and nfl_tscGroups[j].l_boundary < tscGroup.r_boundary:
                    if tscGroup.overlapTSCGroup(nfl_tscGroups[j]):
                        tscGroup.includeNflTSCGroup(nfl_tscGroups[j])
                    j += 1 
                if j > 0: 
                    j -= 1
            splice_ratios.extend(tscGroup.dominantSpliceRatio())
            splice_ranges.extend(tscGroup.spliceSiteRange())
            
        # Process Non Full Length groups 
        if nfl_tscGroups is not None:
            for nfl_group in nfl_tscGroups:
                for idx,nfl_tsc in enumerate(list(nfl_group.transcripts)):
                    if hasattr(nfl_tsc, 'included_by'):
                        overlap,tsc,start_idx = max(nfl_tsc.included_by, key=lambda x:x[0])
                        #nfl_tsc.read_value = 1/len(nfl_tsc.included_by) * (1-nfl_tsc.error_rate)
                        #for overlap,tsc,start_idx in nfl_tsc.included_by:
                        #print("before %f" % tsc.read_value)
                        tsc.transcriptIncludeMerge(nfl_tsc, start_idx)

                        nfl_group.transcripts.remove(nfl_tsc)
                            
            nfl_num_after = countTSCNum(nfl_tscGroups)
            total_nfl += nfl_num_after
            sys.stdout.write("[NON FULL LENGTH]NOTICE: %s has %d to %d transcripts after inclusion\n" 
                             % (chr, nfl_num_before, nfl_num_after) )
        # tscGroup
        for tscGroup in tscGroups:
            tscGroup.filterTranscript()
            output += tscGroup.serializeToGtf(group_num, good_fln_read_count = good_read_count)
            group_num += 1

    end = time()
    sys.stdout.write("[TOTAL NFL]NOTICE: has %d to %d transcripts after inclusion\n" 
                         % (total_nfl_before, total_nfl) )
    print("Process time for transcript discovery from longread: %.2f" % (end-start))
    
    out_gtf = os.path.join(output_dir ,'isolinks_transcripts.gtf')
    #Dumps GTF
    with open(out_gtf, 'w') as outfile:
        for tsc in output:
            outfile.write(tsc+"\n")
    
    # Dumps splice ratios
    '''with open(os.path.join(output_dir , 'splice_ratios.json'), 'w') as outfile:
        json.dump(dict(Counter(splice_ratios)), outfile)
    '''
    # Plot splice ratios
    plt.figure()
    plt.hist(splice_ratios, 50, histtype='bar', rwidth=0.8, log=True)
    plt.xlabel("Dominant splice site ratios", fontsize=14)
    plt.ylabel("log(Count)", fontsize=14)
    plt.savefig(os.path.join(output_dir, "splice_ratios.png"))
    del splice_ratios
    
    '''
    # Plot splice ranges
    with open(os.path.join(output_dir, 'splice_ranges.json'), 'w') as outfile:
        json.dump(dict(Counter(splice_ranges)), outfile)
    '''
    # Plot splice ratios
    plt.figure()
    plt.hist(splice_ranges, 50, histtype='bar', rwidth=0.8, log=True)
    plt.xlabel("Dominant splice site ranges", fontsize=14)
    plt.ylabel("log(Count)", fontsize=14)
    plt.savefig(os.path.join(output_dir, "splice_ranges.png"))
    del splice_ranges
    
    return out_gtf
    
def processGTF(queryfile, annofile, out_dir = "."):
    query = RGTF.ReadGTF(queryfile)
    query.processGTF()
    query.summaryStatistics()
    
    anno = RGTF.ReadGTF(annofile)
    anno.processGTF(style="GENCODE")
    anno.summaryStatistics()
    
    cmp_iso, out_tsc = RGTF.compareGTFTranscripts(query, anno)
    for key in out_tsc:
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)
        RGTF.transcriptDumpToGTF(out_tsc[key], out = os.path.join(out_dir, "iso_" + key + ".gtf"))

if __name__ == "__main__":
    config = configuration()
    out_gtf = processRead(config['fln_path'], config['nfl_path'], output_dir = config['out_dir'])
    #out_gtf = "result/result_20150928_beta_v1/isolinks_transcripts.gtf"
    if os.path.isfile(config['ref_gtf']):
        processGTF(out_gtf, config['ref_gtf'], out_dir = config['out_dir'])
    
    