import sys
import re
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import numpy as np
import argparse
parser = argparse.ArgumentParser()
parser.add_argument("samfile", help="The sam file to process")
parser.add_argument("-outimg", help="The output histogram of map ratio", default=None)
parser.add_argument("-outfasta", help="The output fasta file",default=None)
parser.add_argument("-outsam", help="The output sam file",default=None)
parser.add_argument("-lessthan", help="The max threshold for unidentified reads", type=float,default=1)
parser.add_argument("-largerthan",help="The min threhold for good reads", type=float,default=0)
parser.add_argument('-minlength',help="The min lenght for the read to be counted", type=int,default=0)
parser.add_argument('-quality',help='The min quality scoe for the read to be counted', type=int, default=0)
args = parser.parse_args()

def SamSummary(file):
    global args
    header_flag = 1
    fh = open(file, "r")
    out = {}
    map_ratio = []
    sam = []
    to_del = []
    sequence = {}
    counter = 0
    counter2= 0
    for line in fh:
        line = line.strip()
        if(line[0]=="@"): 
            if header_flag==1: sam.append(line)
            continue 
        else:
            header_flag = 0
        words = line.split("\t")
        if(len(words) < 10): continue
        name,cigar,length = words[0], words[5], len(words[9])
        if length < args.minlength:
            counter+=1
            continue
        cigar_units = re.split(r'[=M]',cigar)
        map_count = 0.0
        for each in cigar_units:
            m = re.search(r'(\d+)$', each)
            if m is None: continue
            map_count += float(m.group(1))
        ratio = map_count/length
        if ratio > 1:
            continue
        map_ratio.append(ratio)     
        if ratio <= args.lessthan and ratio >= args.largerthan: 
                sequence[name] = words[9]
                sam.append(line)
        elif ratio > args.lessthan or ratio < args.largerthan:
                to_del.append(name)
    for each in to_del:
        if each in sequence:
            counter2+=1
            #print(each)
            del sequence[each]
            #print(each+" deleted")
    print("Read count (filtered out by length): "+str(counter))
    print("Read count with multiple mappings (filtered out by mapping ratio error):"+str(counter2))
    out['map_ratio'] = np.array(map_ratio)
    out['sequence']  = sequence
    out['sam'] = sam
    return out

def plotImg(samfile, map_ratio):
     global args
     plt.figure()
     plt.hist(map_ratio, 20, histtype='bar', rwidth=0.8)
     plt.xlabel("Map Ratio", fontsize=14)
     plt.ylabel("Number of reads", fontsize=14)
     plt.savefig(args.outimg)

def writeFasta(sequence):
    global args
    fh = open(args.outfasta,'w')
    for key,value in sequence.iteritems():
        fh.write(">"+key+"\n")
        fh.write(value+"\n")
        
def writeSam(sam):
    global args
    fh = open(args.outsam, 'w')
    for line in sam:
        fh.write(line+"\n")
    
if __name__ == "__main__":
     samfile = args.samfile
     out = SamSummary(samfile)
     map_ratio = out['map_ratio']
     sequence = out['sequence']
     sam = out['sam']
     print("Map ratio count: %d" % len(map_ratio))
     print("Read count: %d" % len(sequence))
     if args.outimg is not None:
         plotImg(samfile, map_ratio)
     if args.outfasta is not None:
         writeFasta(sequence)
     if args.outsam is not None:
         writeSam(sam)
     
     
     