from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import ReadGTF as R
import os
import sys
import re
import json
import matplotlib
import itertools
import copy
from time import time
from collections import Counter
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from operator import itemgetter
PUTATIVE_TN_NUM = 246614
def getRidOfRedundantTSC(GTF):    
    GTF.tsc_num = 0
    GTF.exon_num = 0
    for chr in GTF.chrGroup:
        #print ("###################################################################")
        tscGroups = GTF.chrGroup[chr].findNonOverlappingGroup()
        for tscGroup in tscGroups:
            #sys.stdout.write("[FULL LENGTH]TSC GROUP: %s %d %d\n" % (chr, tscGroup.l_boundary, tscGroup.r_boundary))
            tscGroup.fullLengthTSCFinder()
            GTF.tsc_num += len(tscGroup.anno_transcripts)
            for tsc in tscGroup.anno_transcripts:
                exon_types = []
                GTF.exon_num += len(tsc.exons)
                tsc.status = 'novel'
                tsc.attr['READ_VALUE'] = tsc.read_value
                #for exon in tsc.exons:
                #    exon_types.append(exon.exon_type)
                #print(exon_types)
            tscGroup.transcripts = tscGroup.anno_transcripts
        GTF.chrGroup[chr] = tscGroups
        
if __name__ == "__main__":
    filename = "comparison/PacBio/PacBio_reformatted.gtf"
    query_pacbio = R.ReadGTF(filename)
    query_pacbio.processGTF(style="Pacbio") 
    getRidOfRedundantTSC(query_pacbio)
    query_pacbio.calculateTPM(style="Pacbio", expression='READ_VALUE')
    query_pacbio.summaryStatistics() 
    
    filename = "comparison/GENCODE_23/gencode.v23.chr_patch_hapl_scaff.annotation.sorted.gtf"
    anno = R.ReadGTF(filename)
    anno.processGTF(style="GENCODE")
    anno.summaryStatistics()
    
    cmp_pacbio,out_tsc_pacbio = R.compareGTFTranscripts(query_pacbio, anno)
    
    out_dir = "result/result_20151026_beta_v5"
    for key in out_tsc_pacbio:
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)
        R.transcriptDumpToGTF(out_tsc_pacbio[key], out = os.path.join(out_dir,"pacbio_"+ key + ".gtf"), expression="READ_VALUE")
    
    
    