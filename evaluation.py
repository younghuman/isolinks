from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import ReadGTF as R
import os
import sys
import re
import json
import matplotlib
import itertools
import copy
from time import time
from collections import Counter
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from operator import itemgetter
PUTATIVE_TN_NUM = 246614

def generateROCdata(cmp, anno_num):
    FP, TP = [], []
    true_pos, query_pos = 0,0  
    query_num = len(cmp)
    pre_readvalue = -1
    
    for if_anno,read_value in cmp:
        if if_anno:
            true_pos += 1.0
        query_pos += 1.0
        if read_value != pre_readvalue:
            FP.append((query_pos-true_pos)/PUTATIVE_TN_NUM)
            TP.append(true_pos/anno_num)
            pre_readvalue = read_value
    FP.append((query_pos-true_pos)/PUTATIVE_TN_NUM)
    TP.append(true_pos/anno_num)
    return FP,TP

def plotROC(cmps, labels, anno_num, figname="test.png"):
    linestyles = ['-' , '--' , '-' , '--']
    ls_idx = 0
    color = ['#6677FF', '#FF667F', '#cccc66', '#00b800','#ff3399']
    plt.figure(figsize=(10,10))
    fh = open("roc_data.txt", "w")
    for i,cmp in enumerate(cmps):
        X,Y = generateROCdata(cmp, anno_num)
        fh.write(labels[i]+"_X\t")
        fh.write("\t".join(map(str,X) ) + "\n")
        fh.write(labels[i]+"_Y\t")
        fh.write("\t".join(map(str,Y) ) + "\n")
        
        plt.plot(X, Y,linewidth = 4, color = color[i], ls=linestyles[ls_idx], 
                 label = labels[i] )
        ls_idx = (ls_idx+1)%4
    plt.legend(loc = "upper right",bbox_to_anchor = (1,1), prop={'size':'x-large'}) 
    plt.xlabel('1 - specificity', fontsize=20)
    plt.ylabel('Sensitivity', fontsize= 20)
    plt.xticks(fontsize = 18)
    plt.yticks(fontsize = 18)
    plt.tight_layout()
    plt.savefig(figname)
    fh.close()
    
def getRidOfRedundantTSC(GTF):    
    GTF.tsc_num = 0
    GTF.exon_num = 0
    for chr in GTF.chrGroup:
        tscGroups = GTF.chrGroup[chr].findNonOverlappingGroup()
        for tscGroup in tscGroups:
            #sys.stdout.write("[FULL LENGTH]TSC GROUP: %s %d %d\n" % (chr, tscGroup.l_boundary, tscGroup.r_boundary))
            tscGroup.fullLengthTSCFinder()
            GTF.tsc_num += len(tscGroup.anno_transcripts)
            for tsc in tscGroup.anno_transcripts:
                GTF.exon_num += len(tsc.exons)
                tsc.attr = {}
                tsc.status = 'novel'
                tsc.attr['READ_VALUE'] = tsc.read_value
            tscGroup.transcripts = tscGroup.anno_transcripts
        GTF.chrGroup[chr] = tscGroups
        
      
if __name__ == "__main__":
    filename = "result/result_20151020_beta_v4/isolinks_transcripts.gtf"
    query = R.ReadGTF(filename)
    query.processGTF()
    query.calculateTPM(expression = 'READ_VALUE')
    query.summaryStatistics()
    
    filename = "comparison/cufflinks_short/transcripts.gtf"
    query_cuff_short = R.ReadGTF(filename)
    query_cuff_short.processGTF()
    query_cuff_short.calculateTPM()
    query_cuff_short.summaryStatistics()
    
    filename = "comparison/cufflinks_long/transcripts.gtf"
    query_cuff_long = R.ReadGTF(filename)
    query_cuff_long.processGTF()
    query_cuff_long.calculateTPM()
    query_cuff_long.summaryStatistics()  
    
    filename = "comparison/PacBio/PacBio_reformatted.gtf"
    query_pacbio = R.ReadGTF(filename)
    query_pacbio.processGTF(style="Pacbio") 
    getRidOfRedundantTSC(query_pacbio)
    query_pacbio.calculateTPM(style="Pacbio", expression='READ_VALUE')
    query_pacbio.summaryStatistics() 
    
    filename = "comparison/GENCODE_23/gencode.v23.chr_patch_hapl_scaff.annotation.sorted.gtf"
    anno = R.ReadGTF(filename)
    anno.processGTF(style="GENCODE")
    anno.summaryStatistics()

    cmp_iso, out_tsc = R.compareGTFTranscripts(query, anno)
    cmp_cuff_short, out_cuff = R.compareGTFTranscripts(query_cuff_short, anno, expression = "TPM")
    cmp_cuff_long = R.compareGTFTranscripts(query_cuff_long, anno, expression = "TPM")[0]
    cmp_pacbio,out_tsc_pacbio = R.compareGTFTranscripts(query_pacbio, anno)
    
    out_dir = 'result/result_20151026_beta_v5'
    for key in out_cuff:
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)
        R.transcriptDumpToGTF(out_cuff[key], out = os.path.join(out_dir, "cuff_" + key + ".gtf"), expression="TPM")
        
    for key in out_tsc:
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)
        R.transcriptDumpToGTF(out_tsc[key], out = os.path.join(out_dir,"iso_"+ key + ".gtf"), expression="READ_VALUE")
        
    for key in out_tsc_pacbio:
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)
        R.transcriptDumpToGTF(out_tsc_pacbio[key], out = os.path.join(out_dir,"pacbio_"+ key + ".gtf"), expression="READ_VALUE")
    
    plotROC([cmp_iso, cmp_cuff_short, cmp_cuff_long, cmp_pacbio],
            ['Isolinks','Cufflinks short reads', 'Cufflinks long reads', 'PacBio pipeline']
            , anno.tsc_num, "roc_20151029.png")

