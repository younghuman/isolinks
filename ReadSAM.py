from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import os
import sys
import re
import json
import matplotlib
import itertools
from time import time
from collections import Counter
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import copy
from operator import itemgetter
#Default values for these parameters
_NAME_STRING = "ISOLINKS"
_EXON_IMPRECISION = 3
_FULL_LENGTH_MAX_ERROR = 0.10
_READ_VALUE_MINIMUM = 1.95
_SPLICE_DOMINANT_RATIO_MIN = 0.6
_SPLICE_RANGE_MAX_FOLD = 3
_INTRON_MIN = 50
_EXON_MIN = 10
MILLION = 1000000

def warning(*objs):
    print("WARNING: ", *objs, file=sys.stderr)

class TSCGroup(object):
    """ TSCGroup is a container of a group of Transcripts or AnnotatedTranscripts
        All the transcripts in one TSCGroup have to be in the same Chr
    """
    def __init__(self, l_boundary = None, r_boundary = None):
        self.transcripts = []
        self.l_boundary = l_boundary
        self.r_boundary = r_boundary
        self.read_num   = None
        self.read_value = None

    def findNonOverlappingGroup(self):
        """ Give a TSCGroup, this function returns a list of TSCGroups, 
            where each TSCGroup does not overlap with any other
        """
        tscGroupList = []
        tscGroup = None
        for tsc in self.transcripts:
            if tscGroup == None:
                tscGroup = TSCGroup(l_boundary = tsc.start, r_boundary = tsc.end)
            elif tscGroup.r_boundary <= tsc.start:
                tscGroupList.append(tscGroup)
                tscGroup = TSCGroup(l_boundary = tsc.start, r_boundary = tsc.end)
            elif tsc.end > tscGroup.r_boundary:
                    tscGroup.r_boundary = tsc.end
            tscGroup.transcripts.append(tsc)
            if tsc == self.transcripts[-1]:
                tscGroupList.append(tscGroup)
        return tscGroupList         
    
    
    def fullLengthTSCFinder(self):
        """ I. Given a non-overlapping group of full length reads, 
            this function finds all candidate transcripts,
            save them into list(self.anno_transcripts)
            II. This function will generate all the information for GTF annotation 
            based on full length reads, no assembly is needed but the splicing sites 
            need to be identified correctly. 
            The output should include:
            1) chr
            2) software name (Isolinks)
            3) type (transcript or exon)
            4) start
            5) end
            6) additional information: (transcript_id, read_value, read_num, nfl_read_num)
        """
        self.anno_transcripts = []
        transcripts = self.transcripts
        self.read_num = 0
        self.read_value = 0
        annoTsc = None
        while len(transcripts) > 0:
            while True:
                count = 0
                for tsc in transcripts[:]:
                    if annoTsc == None:
                        annoTsc = AnnotatedTranscript(tsc)
                        transcripts.remove(tsc)
                    else:
                        if annoTsc.transcriptMatch(tsc):
                            annoTsc.transcriptMerge(tsc)
                            transcripts.remove(tsc)
                            count+=1
                if count == 0: 
                    annoTsc.spliceSiteCorrection()
                    annoTsc.updateExonLen()
                    self.anno_transcripts.append(annoTsc)
                    self.read_num += annoTsc.read_num
                    self.read_value += annoTsc.read_value
                    annoTsc = None
                    break
        self.transcripts = self.anno_transcripts
        return
    
    def includeNflTSCGroup(self, tsc_group):
        """ Each transcript in tsc_group is tried to get included into self
        """
        to_include = []
        if not isinstance(tsc_group, TSCGroup):
            raise NameError("This function needs to take a TSCGroup as input!")
        for idx, anno_tsc in enumerate(self.anno_transcripts):
            for i,nfl_tsc in enumerate(tsc_group.transcripts):
                if nfl_tsc.start >= anno_tsc.end:
                    break
                if_include,start_idx = anno_tsc.transcriptInclude(nfl_tsc)
                if if_include:
                    if not hasattr(nfl_tsc, 'included_by'):
                        nfl_tsc.included_by = []
                    nfl_tsc.included_by.append((anno_tsc.overlapLen(nfl_tsc, if_exon=True),anno_tsc,start_idx))
                    
    def overlapTSCGroup(self, tsc_group):
        """ Returns True if self and tsc_group have overlap
        """
        if not isinstance(tsc_group, TSCGroup):
            raise NameError("This function needs to take a TSCGroup as input!")
        if self.l_boundary >= tsc_group.r_boundary or self.r_boundary <= tsc_group.l_boundary:
            return False
        return True
    
    def serializeToGtf(self, group_num = 1, good_fln_read_count = None):
        """ Serialize the TSCGroup to GTF
        """
        output = []
        tsc_num = 1
        for tsc in self.anno_transcripts:
            output += tsc.serializeToGtf(group_num, tsc_num, good_fln_read_count = good_fln_read_count)
            tsc_num += 1
        return output  
    
    def filterTranscript(self):
        """ Filters out the annotated transcript which have low supportive read value,
            or have low quality splice sites
        """
        trash_list = []
        for annoTsc in self.anno_transcripts[:]:
            if not annoTsc.spliceGoodQuality():
                self.anno_transcripts.remove(annoTsc)
            elif annoTsc.read_value <= _READ_VALUE_MINIMUM:
                self.anno_transcripts.remove(annoTsc)
                trash_list.append(annoTsc)
            elif len(annoTsc.exons) <= 1 and annoTsc.read_value < 3 * _READ_VALUE_MINIMUM:
                trash_list.append(annoTsc)
                self.anno_transcripts.remove(annoTsc)
        return
            
    def dominantSpliceRatio(self):
        """ Outputs a list of dominant ratios of each splice site
        """
        if len(self.anno_transcripts) == 0:
            warning("[dominantSpliceRatio] This TSCGroup has no annotated transcripts yet, please run fullLengthTSCFinder() first!")
            return []
        dominant_ratio = []
        for anno_tsc in self.anno_transcripts:
            for exon in anno_tsc.exons:
                if exon.start_range != None:
                    dominant_ratio.append(exon.dominantSpliceRatioStart())
                if exon.end_range != None:
                    dominant_ratio.append(exon.dominantSpliceRatioEnd())
        return dominant_ratio
    
    def spliceSiteRange(self): 
        """ Output a list of ranges of each splice site, range is 1 if there is only one splice site
            Range is the Max(splice sites) - Min(splice sites)+1
        """
        if len(self.anno_transcripts) == 0:
            warning("[spliceSiteRange] This TSCGroup has no annotated transcripts yet, please run fullLengthTSCFinder() first!")
            return []
        splice_ranges = []
        for anno_tsc in self.anno_transcripts:
            for exon in anno_tsc.exons:
                if exon.start_range != None:
                    splice_ranges.append(exon.start_range[1] - exon.start_range[0] -2 * _EXON_IMPRECISION +1)
                if exon.end_range != None:
                    splice_ranges.append(exon.end_range[1] - exon.end_range[0] -2 *_EXON_IMPRECISION +1)   
        return splice_ranges    
                        
    def checkConsistency(self):
        """ TSCGroup can only include transcripts from the same chromosome
            Checks if it is true     
        """
        chr = None
        for tsc in self.transcripts:
            if chr == None:
                chr = tsc.chr
            if chr != tsc.chr:
                return False
        return True 
        
class Exon(object):
    """ Exon is just one putative exon from an alignment
        exon_type can be:
            0-containing both transcript start and end
            1-containing only transcript start
            2-containing only transcript end
            3-containing neither transcript start nor end
            
            getLength(): 
                get the length of the current exon
            matchWith(exon): 
                return True if it overlaps with the exon and all its splice site matches
                return False otherwise
    """
    def __init__ (self, chr, start, end, exon_type=0):
        self.imprecision = _EXON_IMPRECISION
        self.chr = chr
        self.start = start
        self.end = end
        self.exon_type = exon_type
    def getLength(self):
        return self.end - self.start
    
    def matchWith(self, exon):
        """ judge if one exon is totally compatible with another one, this means they 
            1) are of the same exon type
            2) must have overlap
            3) both splice sites agree with the defined error
        """
        if self.exon_type != exon.exon_type:
            return False
        elif self.exon_type != 0:
            if self.start_range != None:
                if (exon.start < self.start_range[0] or 
                    exon.start > self.start_range[1]):
                    return False
            if self.end_range != None :
                if (exon.end < self.end_range[0] or 
                    exon.end > self.end_range[1]):
                    return False
            return True
        else:
            if exon.end <= self.start or exon.start >= self.end:
                return False
            return True
    
    def compatibleWith(self, exon):
        """ 1) If the two exons are different types, return true
            if one exon completely includes or is included by another exon, 
            where the splice sites need to be aligned, 
            2) If the two exons are of the same type, it's the same as compatibleWith()
        """
        if self.exon_type == exon.exon_type:
            return self.matchWith(exon)
        
        #  1)  NNNN  vs  NNNN------
        #  2)  NNNN  vs  ------NNNN
        #  3)  NNNN  vs  ---NNNN---
        if self.exon_type == 0:
            if exon.exon_type == 1:
                if self.end > exon.start and self.end <= exon.end_range[1]:
                    return True
            elif exon.exon_type == 2:
                if self.start >= exon.start_range[0] and self.start < exon.end:
                    return True
            elif exon.exon_type == 3:
                if self.start >= exon.start_range[0] and self.end <= exon.end_range[1]:
                    return True
                
        #   1) NNNN------  vs  NNNN
        #   2) NNNN------  vs  ------NNNN
        #   3) NNNN------  vs  ---NNNN---
        elif self.exon_type == 1:
            if exon.exon_type == 0:
                if exon.end > self.start and exon.end <= self.end_range[1]:
                    return True
            elif exon.exon_type == 2:
                if (self.start < exon.end and self.start >= exon.start_range[0] and
                    exon.end <= self.end_range[1]):
                    return True
            elif exon.exon_type == 3:
                if (self.start >= exon.start_range[0] and 
                    self.end >= exon.end_range[0] and self.end <= exon.end_range[1]):
                    return True
                
        #   1) ------NNNN  vs  NNNN
        #   2) NNNN------  vs  ------NNNN
        #   3) NNNN------  vs  ---NNNN---        
        elif self.exon_type == 2:
            if exon.exon_type == 0:
                if exon.start >= self.start_range[0] and exon.start < self.end:
                    return True
            elif exon.exon_type == 1:
                if (self.end > exon.start and self.end <= exon.end_range[1] and
                    exon.start >= self.start_range[0]):
                    return True
            elif exon.exon_type == 3:
                if (self.start >= exon.start_range[0] and self.start <= exon.start_range[1] and
                    self.end <= exon.end_range[1]):
                    return True
        #   1) ---NNNN---  vs  NNNN
        #   2) ---NNNN---  vs  ------NNNN
        #   3) ---NNNN---  vs  ---NNNN---
        elif self.exon_type == 3:
            if exon.exon_type == 0:
                if exon.start >= self.start_range[0] and exon.end <= self.end_range[1]:
                    return True
            elif exon.exon_type == 1:
                if (exon.start >= self.start_range[0] and
                    exon.end >= self.end_range[0] and exon.end <= self.end_range[1]):
                    return True
            elif exon.exon_type == 2:
                if (exon.end <= self.end_range[1] and
                    exon.start >= self.start_range[0] and exon.start <= self.start_range[1]):
                    return True
        return False
    @property    
    def exon_type(self):
        return self._exon_type
    @exon_type.setter
    def exon_type(self,exon_type):
        if exon_type == 0:
            self.start_range = None
            self.end_range = None
        elif exon_type == 1:
            self.start_range = None
            self.end_range = (self.end-self.imprecision, self.end+self.imprecision)
        elif exon_type == 2:
            self.start_range = (self.start-self.imprecision, self.start+self.imprecision)
            self.end_range = None
        elif exon_type == 3:
            self.start_range = (self.start-self.imprecision, self.start+self.imprecision)
            self.end_range = (self.end-self.imprecision, self.end+self.imprecision)
        else:
            raise ValueError('exon_type has to be 0,1,2 or 3')   
        self._exon_type = exon_type

class AnnotatedExon(Exon):
    '''This is a class for annotated exon, ready for serialization into GTF file'''
    def __init__ (self, exon):
        if not isinstance(exon, Exon):
            raise NameError("The AnnotatedExon can be only initialized from an Exon!")
        self.chr = exon.chr
        self.start = exon.start
        self.end = exon.end
        self.startCounter = Counter([self.start])
        self.endCounter = Counter([self.end])
        self.exon_type = exon.exon_type
        self.imprecision = exon.imprecision
        if hasattr(exon, 'orig'):
            self.orig = exon.orig
        if exon.start_range is not None:
            self.start_range = list(exon.start_range)
        if exon.end_range is not None:
            self.end_range = list(exon.end_range)
   
    def exonMerge(self, exon):
        """ Merges an Exon into this Annotated exon, the ranges of start and end will
            be extended
        """
        if not isinstance(exon, Exon):
            raise NameError("The exonMerge needs to take an Exon!")
        if exon.start_range is not None:
            if exon.start_range[0] < self.start_range[0]:
                self.start_range[0] = exon.start_range[0]
            if exon.start_range[1] > self.start_range[1]:
                self.start_range[1] = exon.start_range[1]
            self.startCounter += Counter([exon.start])
        if exon.end_range is not None:
            if exon.end_range[0] < self.end_range[0]:
                self.end_range[0] = exon.end_range[0]
            if exon.end_range[1] > self.end_range[1]:
                self.end_range[1] = exon.end_range[1]
            self.endCounter += Counter([exon.end])
        
    def serializeToGtf(self, group_num = 1, num = 1, exon_id = 1):
        """ Serialize the annotated exon into GTF format
        """
        gene_id = 'gene_id "%s.%d"' %(_NAME_STRING, group_num)
        transcript_id = 'transcript_id "%s.%d.%d"' %(_NAME_STRING, group_num, num)
        exon_number ='exon_number "%d"' % exon_id
        exon_type_str = 'exon_type "%d"' % self.exon_type
        anno_string = "; ".join([gene_id, transcript_id, exon_number, exon_type_str])
        exon_string =  "\t".join([self.chr, _NAME_STRING, "exon",str(self.start), 
                          str(self.end-1), ".", ".",".", anno_string])
        return exon_string
     
    def dominantSpliceRatioStart(self):
        return self.startCounter.most_common(1)[0][1]/sum(self.startCounter.values())   
    
    def dominantSpliceRatioEnd(self):
        return self.endCounter.most_common(1)[0][1]/sum(self.endCounter.values())
    
    def dominantSpliceStart(self):
        return self.startCounter.most_common(1)[0][0]
    
    def dominantSpliceEnd(self):
        return self.endCounter.most_common(1)[0][0]
    @property    
    def exon_type(self):
        return self._exon_type
    @exon_type.setter
    def exon_type(self,exon_type):
        if exon_type == 0:
            self.start_range = None
            self.end_range = None
        elif exon_type == 1:
            self.start_range = None
        elif exon_type == 2:
            self.end_range = None
        elif exon_type != 3:
            raise ValueError('exon_type has to be 0,1,2 or 3')   
        self._exon_type = exon_type
      
class Transcript(object):
    ''' Transcript class is a long transcript including all the exons and long reads
    '''
    def __init__ (self,chr, start, end, read_num = 1):
        self.chr = chr
        self.start = start
        self.end = end
        self.exons = []
        self.exon_len = 0
        self.error_rate = 0
        self.read_num = read_num
        
    def getSpan(self):
        return self.end - self.start 
    
    def overlapRate(self, tsc, if_exon = False):
        """ returns the ratio of itself which overlaps with tsc
        """
        con_start = None
        con_end = None
        if self.end <= tsc.start or self.start >= tsc.end:
            return 0
        else:
            if self.start > tsc.start:
                con_start = self.start
            else:
                con_start = tsc.start
            if self.end < tsc.end:
                con_end = self.end
            else:
                con_end = tsc.end
            # If it is not comparison of exonic length, directly return    
            if if_exon == False:
                con_len = con_end - con_start
                return con_len/self.getSpan()
            # If it is comparison of exonic length, returns the overlapping exon_length/self.exon_len
            else:
                con_len = 0
                for exon in self.exons:
                    if exon.end <= con_start or exon.start >= con_end:
                        continue
                    else:
                        if exon.end < con_end:
                            exon_end = exon.end
                        else:
                            exon_end = con_end
                        if exon.start < con_start:
                            exon_start = con_start
                        else:
                            exon_start = exon.start
                        con_len += exon_end-exon_start
                return con_len/self.exon_len
            
    def overlapLen(self, tsc, if_exon = False):
        """ returns the length of self which overlaps with tsc
        """
        con_start = None
        con_end = None
        if self.end <= tsc.start or self.start >= tsc.end:
            return 0
        else:
            if self.start > tsc.start:
                con_start = self.start
            else:
                con_start = tsc.start
            if self.end < tsc.end:
                con_end = self.end
            else:
                con_end = tsc.end
            # If it is not comparison of exonic length, directly return    
            if if_exon == False:
                con_len = con_end - con_start
                return con_len
            # If it is comparison of exonic length, returns the overlapping exon_length/self.exon_len
            else:
                con_len = 0
                for exon in self.exons:
                    if exon.end <= con_start or exon.start >= con_end:
                        continue
                    else:
                        if exon.end < con_end:
                            exon_end = exon.end
                        else:
                            exon_end = con_end
                        if exon.start < con_start:
                            exon_start = con_start
                        else:
                            exon_start = exon.start
                        con_len += exon_end-exon_start
                return con_len
            
    def transcriptMatch(self, tsc):
        """ returns true if self matches with tsc, otherwise return false
        """
        if len(self.exons) != len(tsc.exons):
            return False
        for exon1, exon2 in zip(self.exons, tsc.exons):
            if not exon1.matchWith(exon2):
                return False
        return True
    
    def transcriptInclude(self, tsc):
        """ 1) Returns true if self includes tsc, which means from one exon in self, 
            each continuous exon is compatible with each continuous exon in tsc,
            until the last exon of tsc
            2) self should be a full length annotated transcript
            3) tsc should be a non-full-length transcript
        """
        start_idx = None
        idx_2 = 0
        if len(self.exons) < len(tsc.exons) or self.start >= tsc.end or self.end <= tsc.start:
            return (False, start_idx)
        
        for (idx, exon) in enumerate(self.exons):
            if start_idx is not None:
                if idx_2 >= len(tsc.exons):
                    return (True, start_idx)
                elif exon.compatibleWith(tsc.exons[idx_2]):
                    idx_2 += 1
                    continue
                else:
                    return (False, start_idx)
            elif exon.compatibleWith(tsc.exons[0]):
                start_idx = idx
                idx_2 += 1
                continue
            elif ((tsc.exons[0].end_range is None and exon.start >= tsc.exons[0].end) or 
                  (tsc.exons[0].end_range is not None and exon.start >= tsc.exons[0].end_range[1])):
                return (False, start_idx)
        # self.exons finishes iteration, idx_2 is just the end index of tsc.exons
        if idx_2 >= len(tsc.exons):
            return (True, start_idx)
        # otherwise the tsc.exons has not reached the end, it's not an inclusion
        else:
            return (False, start_idx)
    
    def transcriptPointTo(self, tsc):
        """ Returns true if self points to tsc, which means from one exon in self, 
            each continuous exon is compatible with each continuous exon in tsc,
            until the last exon of self
        """
        start_idx = None
        idx_2 = 0
        if self.start >= tsc.end or self.end <= tsc.start:
            return (False, start_idx)
        
        for (idx, exon) in enumerate(self.exons):
            if start_idx is not None:
                if idx_2 >= len(tsc.exons):
                    return (False, start_idx)
                elif exon.compatibleWith(tsc.exons[idx_2]):
                    idx_2 += 1
                    continue
                else:
                    return (False, start_idx)
            elif exon.compatibleWith(tsc.exons[0]):
                start_idx = idx
                idx_2 += 1
                continue
            elif ((tsc.exons[0].end_range is None and exon.start >= tsc.exons[0].end) or 
                  (tsc.exons[0].end_range is not None and exon.start >= tsc.exons[0].end_range[1])):
                return (False, start_idx)
            
        return (True, start_idx)
    def updateExonLen(self):
        self.exon_len = 0
        for exon in self.exons:
            self.exon_len += exon.getLength()
    def updateExonType(self):
        """ Update exon types of each exon in this transcript
        """
        length = len(self.exons)
        if length == 1:
            self.exons[0].exon_type = 0
        else:
            for idx,exon in enumerate(self.exons):
                if idx == 0:
                    self.exons[idx].exon_type = 1
                elif idx == length-1:
                    self.exons[idx].exon_type = 2
                else:
                    self.exons[idx].exon_type = 3
        return
                    
    def checkConsistency(self):
        """ Checks if the values of this Transcript is valid
        """
        tsc_length = self.end-self.start+1
        exon_span = self.exons[-1].end - self.exons[0].start + 1
        if tsc_length == exon_span:
            return True
        else:    
            return False        

class AnnotatedTranscript(Transcript):
    '''This is a class for annotated transcript, ready for serialization into GTF file'''
    def __init__ (self, tsc, read_num=1):
        if not isinstance(tsc, Transcript):
            raise NameError("The AnnotatedTranscript can be only initialized from a Transcript!")
        self.score = None
        self.FPM = None
        self.error_rate = tsc.error_rate
        self.read_num = read_num
        self.nfl_read_num = 0
        self.read_value = read_num *(1- self.error_rate)
        self.chr = tsc.chr
        self.start = tsc.start
        self.end = tsc.end 
        self.exons = [AnnotatedExon(exon) for exon in tsc.exons]
        self.exon_len = tsc.exon_len
        if hasattr(tsc, 'orig'):
            self.orig = tsc.orig
        if hasattr(tsc, 'attr'):
            self.attr = copy.deepcopy(tsc.attr)
        
    
    def serializeToGtf(self, group_num = 1, num = 1, good_fln_read_count = None):
        output = []
        gene_id = 'gene_id "%s.%d"' %(_NAME_STRING,group_num)
        transcript_id = 'transcript_id "'+ _NAME_STRING +'.%d.%d"' %(group_num, num)
        anno_string = "; ".join([gene_id, transcript_id,
                                 'READ_NUM "%d"' % self.read_num,
                                 'NFL_READ_NUM "%d"' % self.nfl_read_num,
                                 'READ_VALUE "%.4f"' % self.read_value ])
        if isinstance(good_fln_read_count, int):
            anno_string += '; TPM "%.4f"' % (self.read_value/good_fln_read_count * MILLION)
        tsc_string =  "\t".join([self.chr, _NAME_STRING, "transcript",str(self.start), 
                          str(self.end-1), ".", ".",".", anno_string])
        output.append(tsc_string)
        for idx,exon in enumerate(self.exons):
            output.append(exon.serializeToGtf(group_num, num, idx+1))
        return output

    def transcriptMerge(self, tsc):
        """ Merges a matched Transcript into this AnnotatedTranscript
        """
        if not isinstance(tsc, Transcript):
            raise NameError("transcriptMerge needs to take a Transcript!")
        for i, (exon1, exon2) in enumerate(zip(self.exons, tsc.exons)):
            self.exons[i].exonMerge(exon2)
        if self.start > tsc.start:
            self.start = tsc.start
            self.exons[0].start = tsc.start
        if self.end < tsc.end:
            self.end = tsc.end
            self.exons[-1].end = tsc.end
        self.read_num += tsc.read_num
        if hasattr(tsc, 'read_value'):
            self.read_value += tsc.read_value
        else:
            self.read_value += 1 * (1-tsc.error_rate)
    
    def transcriptIncludeMerge(self, tsc, start_idx):
        """ self has to include tsc, to save time, it is not asserted here
            from start_idx, each exon of self will merge with each exon in tsc
        """
        i = start_idx
        j = 0
        while j < len(tsc.exons):
            self.exons[i].exonMerge(tsc.exons[j])
            i += 1
            j += 1
        self.nfl_read_num += tsc.read_num
        if hasattr(tsc, 'read_value'):
            self.read_value += tsc.read_value * self.overlapRate(tsc, if_exon=True)
        else:
            value = (1-tsc.error_rate) * self.overlapRate(tsc, if_exon=True)
            assert value <= 1
            self.read_value += value
        
    
    def spliceSiteCorrection(self):
        """ Correct each splice site to be the dominant site
        """
        for i in range(len(self.exons)):
            if self.exons[i].start_range is not None:
                self.exons[i].start = self.exons[i].dominantSpliceStart()
            if self.exons[i].end_range is not None:
                self.exons[i].end = self.exons[i].dominantSpliceEnd()
     
    
    def spliceGoodQuality(self):
        """ Returns false if the transcript has low quality splice sites 
        """
        for exon in self.exons:
            if exon.start_range != None:
                ratio = exon.dominantSpliceRatioStart()
                range = exon.start_range[1] - exon.start_range[0] - 2*_EXON_IMPRECISION +1 
                if ratio < _SPLICE_DOMINANT_RATIO_MIN or range > _SPLICE_RANGE_MAX_FOLD * _EXON_IMPRECISION + 1:
                        return False
            if exon.end_range != None:
                ratio = exon.dominantSpliceRatioEnd()
                range = exon.end_range[1] - exon.end_range[0] - 2*_EXON_IMPRECISION +1 
                if ratio < _SPLICE_DOMINANT_RATIO_MIN or range > _SPLICE_RANGE_MAX_FOLD * _EXON_IMPRECISION + 1:
                        return False

        return True
class ReadSAM(object):
    ''' ReadSAM takes a SAM file name as input,
        then processes the SAM file into transcripts and exons
    '''
    def __init__(self, filename, intron_min = _INTRON_MIN, if_MD_tag = True,
                 error_max = _FULL_LENGTH_MAX_ERROR):
        ''' Arguments:
            1) filename: the file name of sorted sam file
            2) intron_min: the minimal length for an intron to be counted
            3) max_error_rate: the maximal error rate which can be tolerated
            4) if_MD_tag: if the @MD tag is present in the SAM file 
        '''   
        self.total_reads = 0
        self.error_reads = 0
        self.error_max = error_max
        self.chrGroup = {}
        self.intron_min = intron_min
        self.if_MD_tag = if_MD_tag
        self.fh = open(filename)
    
    def summaryStatistics(self):
        """ Prints the summary statistics of the current SAM read      
        """
        #sys.stderr.write("NOTICE: summaryStatistics() starts!\n")
        if not self.chrGroup:
            warning("The SAM file is vacant or illegal!")
            return
        print("-------------------------- Summary of error reads ----------------------------")
        print("Num of Total Reads: %d" % self.total_reads)
        print("Num of Good Reads: %d" % (self.total_reads-self.error_reads))
        print("Num of Error Reads: %d" % self.error_reads)
        print("Ratio of Error Reads: %.4f" % (self.error_reads/self.total_reads) )
        print("------------------------------------------------------------------------------")
            
    def processSAM(self):
        """process the SAM file into TSCGroups saved by key chromosome in self.chrGroup        
        """
        p_start = 0
        for line in self.fh:
            if line[0] == '@': 
                continue
            line = line.strip('\r')
            line = line.strip('\n')
            words = re.split(r"\t", line)
            if len(words) < 6:
                continue
            (seqname, chr, start, cigar) = itemgetter(*[0,2,3,5])(words)
            tags = "\t".join(words[11:-1])
            start = int(start)
            if chr not in self.chrGroup:
                p_start = 0
                self.chrGroup[chr] = TSCGroup()
            if start < p_start:
                raise NameError('The sam file is not sorted!')
            self.processCigar(chr, start, cigar, seqname, tags)
            p_start = start
        self.fh.close()
        
    def countMDMismatch(self, MD):   
        """ counts the number of mismatches from MD tag    
        """ 
        mismatch = re.findall(r'(?:^|\d)([ATCG]+)', MD)
        mismatch = ''.join(mismatch)
        return len(mismatch)
    
    def processCigar(self,chr,start, cigar, seqname, tags):
        '''Process Cigar into exons and a single transcript
        '''
        match = re.findall(r'(\d+)(\w)', cigar)
        exon_start,exon_end = start,start
        #if_begin indicates if it is the beginning stage (no exons found yet)
        if_begin, exon_type = 1, 1
        deletion_length,insert_length = 0, 0
        exon_length = 0
        cur_pos = start
        transcript = Transcript(chr,start, start)
        #Find the last segment of match
        for idx,pair in list(enumerate(reversed(match))):
            if pair[1] == 'M' or pair[1] == '=' or pair[1] == 'X':
                break
        last_match_idx = len(match)-idx-1
        for idx,pair in enumerate(match):
            pair = list(pair)
            pair[0] = int(pair[0])
            #The last exon
            if idx == last_match_idx:
                exon_end += pair[0]
                if exon_type==1:
                    exon_type=0
                elif exon_type==3:
                    exon_type=2
                segment_len = exon_end - exon_start
                exon_length += segment_len
                if segment_len <= _EXON_MIN:
                    self.error_reads +=1
                    self.total_reads += 1
                    return
                exon = Exon(chr,exon_start,exon_end,exon_type=exon_type)
                transcript.exons.append(exon)
                transcript.end = transcript.exons[-1].end
                transcript.exon_len = exon_length
                self.total_reads += 1
                self.chrGroup[chr].transcripts.append(transcript)
                break
            #Process each tag in Cigar string
            if pair[1] == 'M' or pair[1] == '=' or pair[1] == 'X':
                if if_begin == 1:
                    if_begin = 0
                exon_end += pair[0]
                cur_pos  += pair[0]
            elif pair[1] == 'I':
                if if_begin == 1 or idx > last_match_idx:
                    continue
                insert_length += pair[0]
            elif pair[1] == 'D' or pair[1] == 'N':
                if if_begin == 1 or idx > last_match_idx: 
                    continue
                cur_pos += pair[0]
                if pair[0] < self.intron_min:
                    exon_end += pair[0]
                    deletion_length += pair[0]
                #Find an exon, we should construct it
                else:
                    #from initial type to containing transcript start
                    segment_len = exon_end - exon_start
                    exon_length += segment_len
                    if segment_len <= _EXON_MIN:
                        self.error_reads +=1
                        self.total_reads += 1
                        return
                    exon = Exon(chr,exon_start,exon_end,exon_type=exon_type)
                    transcript.exons.append(exon)
                    exon_start = exon_end+pair[0]
                    exon_end = exon_start
                    exon_type = 3
        mismatch = 0
        if self.if_MD_tag:
            MD_match = re.match(r'MD:Z:(.+?)\t', tags)
            if MD_match:
                MD = MD_match.group(1)
                mismatch = self.countMDMismatch(MD)
            else:
                raise NameError('The MD tag is not present!')
        error_rate = (deletion_length + insert_length + mismatch)/exon_length
        if error_rate > self.error_max:
            self.error_reads += 1
            del(self.chrGroup[chr].transcripts[-1])
        else:
            self.chrGroup[chr].transcripts[-1].error_rate = error_rate
        return 


    
    
    
                