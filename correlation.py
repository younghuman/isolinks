from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import ReadGTF as R
import os
import sys
import re
import json
import matplotlib
import itertools
import copy
from time import time
from collections import Counter
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import scipy.stats as stats
import numpy as np
from operator import itemgetter
def getStatistics(query, exon_num, tsc_len):
    for chr in query.chrGroup:
        for tsc in query.chrGroup[chr]:
            np.append(exon_num, len(tsc.exons))
            np.append(tsc_len, tsc.exon_len)
    return

def plotCorrelationTSC(annotated_tsc, thres = [0,2,5,10]):
    
    for thre in thres:
        tpm_a = np.array([])
        tpm_b = np.array([])
        
        for tsc in annotated_tsc:
            tpm_a_val = float(tsc.attr['TPM'])
            tpm_b_val = float(tsc.attr['TPM_CUFF'])
            if len(tsc.exons) >= thre:
                tpm_a = np.append(tpm_a, tpm_a_val)
                tpm_b = np.append(tpm_b, tpm_b_val)
        plotCorrelation(tpm_a, tpm_b, thre = thre)

def plotCorrelationGene(query_a, query_b, thres = [0,2,5,10]):
    for thre in thres:
        tpm_a, tpm_b = np.array([]), np.array([])
        gene_info_a = query_a.getGeneInfo(thre)
        gene_info_b = query_b.getGeneInfo(thre)
        concordant_genes = set(gene_info_a.keys()) & set(gene_info_b.keys())
        for gene in concordant_genes:
            tpm_a = np.append(tpm_a, gene_info_a[gene])
            tpm_b = np.append(tpm_b, gene_info_b[gene])
        plotCorrelation(tpm_a,tpm_b, thre = thre, name="analysis/correlation_gene")

def plotCorrelation(tpm_a,tpm_b, thre = 0, label_x = 'Isolinks TPM', label_y = 'Cufflinks TPM', 
                    name="analysis/correlation_iso"):
        slope, intercept, r_value, p_value, std_err = stats.linregress(tpm_a, tpm_b)
        with open(name + "_%d.txt" % thre, 'w') as fh:
            fh.write("slope: %f\nintercept: %f\ncorrelation: %f\np_value: %f\nstderr: %f" %
                     (slope, intercept, r_value, p_value, std_err))
            
        x = np.linspace(0.1,100000, 100000)
        y = slope * x + intercept
        title = "line:%.4fx + %.4f  correlation:%.4f (>=%d exons)" % (slope, intercept, r_value, thre)
        
        plt.figure(figsize=(10,10))
        plt.scatter(tpm_a, tpm_b, marker='o',s=10, color='#6677FF', alpha=0.7)
        plt.plot(x, y, '-', color="#000000")
        plt.title(title, fontsize = 24, y= 1.05)
        plt.xlabel(label_x, fontsize=20)
        plt.ylabel(label_y, fontsize= 20)
        plt.xticks(fontsize = 18)
        plt.yticks(fontsize = 18)
        plt.xscale('log')
        plt.yscale('log')
        plt.savefig(name + "_%d.png" % thre)

if __name__ == "__main__":
    #####################################################
    #         Plot correlation at isoform level         #
    #####################################################
    filename = "result/result_20151026_beta_v5/isolinks_transcripts.gtf"
    query = R.ReadGTF(filename)
    query.processGTF()
    query.calculateTPM(expression='READ_VALUE')
    query.summaryStatistics()
    
    filename = "comparison/cufflinks_short/transcripts.gtf"
    query_cuff_short = R.ReadGTF(filename)
    query_cuff_short.processGTF()
    query_cuff_short.calculateTPM(expression = 'FPKM', tpm_name ='TPM_CUFF')
    query_cuff_short.summaryStatistics()
    
    cmp_iso_cuff, out_tsc_iso_cuff = R.compareGTFTranscripts(query, query_cuff_short)
    
    R.transcriptDumpToGTF(out_tsc_iso_cuff['annotated'], out = os.path.join("correlation", "annotated_iso_cuff.gtf"))
    annotated_tsc = out_tsc_iso_cuff['annotated']
    plotCorrelationTSC(annotated_tsc)
   
    #####################################################
    #         Plot correlation at gene level         #
    #####################################################
    file1 = "result/result_20151026_beta_v5/iso_annotated.gtf"
    query_anno = R.ReadGTF(file1)
    query_anno.processGTF()
    
    file2 = "result/result_20151026_beta_v5/iso_isoforms.gtf"
    query_iso = R.ReadGTF(file2)
    query_iso.processGTF()
    
    query_anno.combineWith(query_iso)
    query_anno.summaryStatistics()
    
    file1 = "result/result_20151026_beta_v5/cuff_annotated.gtf"
    query_cuff_anno = R.ReadGTF(file1)
    query_cuff_anno.processGTF()
    
    file2 = "result/result_20151026_beta_v5/cuff_isoforms.gtf"
    query_cuff_iso = R.ReadGTF(file2)
    query_cuff_iso.processGTF()
    
    query_cuff_anno.combineWith(query_cuff_iso)
    query_cuff_anno.summaryStatistics()
    
    plotCorrelationGene(query_anno, query_cuff_anno)
    

    
    
    