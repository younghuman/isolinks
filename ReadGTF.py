from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import os
import sys
import re
import json
import matplotlib
import itertools
import copy
from time import time
from collections import Counter, defaultdict
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from operator import itemgetter
import ReadSAM as R
R._EXON_IMPRECISION = 0
_OVERLAP_MIN = 0.10
MILLION = 10**6
def warning(*objs):
    print("WARNING: ", *objs, file=sys.stderr)

def transcriptDumpToGTF(tsclist, out = "test.gtf", expression="READ_VALUE"):
    tsclist = sorted(tsclist, key = lambda x:float(x.attr[expression]), reverse = True)
    with open(out,'w') as fh:
        for tsc in tsclist:
            anno_list = []
            for key in sorted(tsc.attr.keys()):
                anno_list.append('%s "%s"' %(key, str(tsc.attr[key]) ))
            anno_str = "; ".join(anno_list)
            fh.write("\t".join([tsc.orig, anno_str]) + "\n")
            for exon in tsc.exons:
                fh.write(exon.orig + "\n")

def transcriptAttribute(query_tsc, anno_tsc):
    if 'gene_name' in anno_tsc.attr:
        query_tsc.attr['gene_name'] = anno_tsc.attr['gene_name']
    if 'gene_id' in anno_tsc.attr:
        query_tsc.attr['gene_id'] = anno_tsc.attr['gene_id']
    if 'gene_status' in anno_tsc.attr:
        query_tsc.attr['gene_status'] = anno_tsc.attr['gene_status']
    if 'gene_biotype' in anno_tsc.attr:
        query_tsc.attr['gene_biotype'] = anno_tsc.attr['gene_biotype']
    if 'level' in anno_tsc.attr:
        query_tsc.attr['level'] = anno_tsc.attr['level']
    if 'FPKM' in anno_tsc.attr:
        query_tsc.attr['FPKM'] = anno_tsc.attr['FPKM']
    if 'TPM' in anno_tsc.attr and 'TPM' in query_tsc.attr:
        query_tsc.attr['TPM'] = min(query_tsc.attr['TPM'], anno_tsc.attr['TPM'])
    if 'TPM_CUFF' in anno_tsc.attr:
        query_tsc.attr['TPM_CUFF'] = anno_tsc.attr['TPM_CUFF']
    return query_tsc

def findAnnotatedTranscripts(query_group, anno_group):
    """ This function takes two TSCGroup classes, outputs:
         1) A list of Transcripts containing all annotated transcripts in query (matched with a known transcript)
         2) A list of Transcripts containing all novel isoforms in query (non-annotated transcripts and at least 10% of itself 
                                                                        overlaps with a known transcript)
         3) A list of Transcripts containing all transcripts in query (non-annotated transcripts, not satisfying novel isoform standard)
    """
    if not isinstance(query_group, R.TSCGroup) or not isinstance(anno_group, R.TSCGroup):
        raise NameError("findAnnotatedTranscripts should take two TSCGroup object!")
    for (i, query_tsc) in enumerate(query_group.transcripts):
        max_overlap, max_tsc = 0, None
        for (j, anno_tsc) in enumerate(anno_group.transcripts):
            if query_tsc.transcriptMatch(anno_tsc):
                query_tsc = transcriptAttribute(query_tsc, anno_tsc)
                query_tsc.status = 'annotated'
                query_tsc.attr['OVERLAP'] = 1
                break
            else:
                overlap = query_tsc.overlapRate(anno_tsc)
                if overlap > max_overlap:
                    max_overlap = overlap
                    max_tsc = anno_tsc
            # The annotation transcript is out of range, thus no need to iterate anymore
            if query_tsc.end <= anno_tsc.start or j == len(anno_group.transcripts)-1:
                if query_tsc.status != 'annotated':
                    query_tsc.attr['OVERLAP'] = max_overlap
                if max_overlap >= _OVERLAP_MIN:
                    query_tsc = transcriptAttribute(query_tsc, max_tsc)
                    if query_tsc.status != 'annotated':
                        query_tsc.status = 'isoforms'
                max_tsc = None
                break
            
    return query_group

def compareGTFTranscripts(query, anno, expression = "READ_VALUE"):
    """ This function takes two ReadGTF classes, outputs:
        cmp_result -  A list of comparison result, from which each record is a tuple (if_annotated, read_value or FPKM)
        out_tsc - A hash with three keys: annotated, novel, isoforms:
        1) A list of Transcripts containing all annotated transcripts in query (matched with a known transcript)
        2) A list of Transcripts containing all novel isoforms in query (non-annotated transcripts and at least 10% of itself 
                                                                        overlaps with a known transcript)
        3) A list of Transcripts containing all transcripts in query (non-annotated transcripts, not satisfying novel isoform standard)
    """ 
    if not isinstance(anno, ReadGTF) or not isinstance(query, ReadGTF):
        raise NameError("compareGTFTranscripts should take two ReadGTF object!")
    cmp_result = []
    out_tsc = {'annotated':[], 'isoforms':[], 'novel': [] }
    for chr in query.chrGroup:
        # Allows two types of input, both one TSCGroup or clustered TSCGroups
        if chr in anno.chrGroup:
            if isinstance(anno.chrGroup[chr], R.TSCGroup):
                anno_groups = anno.chrGroup[chr].findNonOverlappingGroup()
            elif isinstance(anno.chrGroup[chr], list):
                anno_groups = anno.chrGroup[chr]
            else:
                raise NameError("Wrong input type!")
            
        if isinstance(query.chrGroup[chr], R.TSCGroup):
            query_groups = query.chrGroup[chr].findNonOverlappingGroup()
        elif isinstance(query.chrGroup[chr], list):
            query_groups = query.chrGroup[chr]
        else:
            raise NameError("Wrong input type!")
        # If annotation database does not have this chr, directly categorize it into novel
        if chr not in anno.chrGroup:
            for tscGroup in query_groups:
                out_tsc['novel'].extend(tscGroup.transcripts)                
            continue
            
        j = 0
        for i,query_group in enumerate(query_groups):
            while j < len(anno_groups) and anno_groups[j].l_boundary < query_group.r_boundary:
                if query_group.overlapTSCGroup(anno_groups[j]):
                     query_group = findAnnotatedTranscripts(query_group, anno_groups[j])
                j += 1 
            if j > 0: 
                j -= 1
            for tsc in query_group.transcripts:
                out_tsc[tsc.status].append(tsc)

    # Generate the comparison result statistics in (if_annotated, FPKM or READ_VALUE) format
    for tsc in out_tsc['novel']:
        if expression not in tsc.attr:
            raise NameError("Transcript does not have %s attribute!" % expression)
        cmp_result.append((False, tsc.attr[expression]))
        tsc.status = 'novel'

    for tsc in out_tsc['isoforms']:
        if expression not in tsc.attr:
            raise NameError("Transcript does not have %s attribute!" % expression)
        cmp_result.append((False, tsc.attr[expression]))
        tsc.status = 'novel'

    for tsc in out_tsc['annotated']:
        if expression not in tsc.attr:
            raise NameError("Transcript does not have %s attribute!" % expression)
        cmp_result.append((True, tsc.attr[expression]))
        tsc.status = 'novel'

    cmp_result = sorted(cmp_result, key=lambda each: float(each[1]), reverse = True)
    return cmp_result, out_tsc


class ReadGTF(object):
    ''' ReadSAM takes a SAM file name as input,
        then processes the SAM file into transcripts and exons
    '''
    def __init__(self, filename):
        ''' Arguments:
            1) filename: the GTF file as input
        '''   
        self.exon_num = 0
        self.tsc_num = 0
        self.chrGroup = {}
        self.fh = open(filename)
    
    def summaryStatistics(self):
        """ Prints the summary statistics of the current SAM read      
        """
        if not self.chrGroup:
            warning("The GTF file is vacant or illegal!")
            return
        print("-------------------------- Summary of GTF transcripts ----------------------------")
        print("Num of Total Transcripts: %d" % self.tsc_num)
        print("Num of Total Exons: %d" % self.exon_num)
        print("------------------------------------------------------------------------------")
    
    def sort(self):
        for chr in self.chrGroup:
            self.chrGroup[chr].transcripts = sorted(self.chrGroup[chr].transcripts,
                                                    key = lambda each: (each.start, each.end))
    def calculateTPM(self, expression = 'FPKM', tpm_name = 'TPM', style ='usual'):
        total_FPKM = 0
        if style != 'Pacbio':
            for chr in self.chrGroup:
                for tsc in self.chrGroup[chr].transcripts:
                    total_FPKM += float(tsc.attr[expression])
                
            for chr in self.chrGroup:
                for tsc in self.chrGroup[chr].transcripts:
                    tsc.attr[tpm_name] = float(tsc.attr[expression])/total_FPKM * MILLION
        else:
            for chr in self.chrGroup:
                for tscGroup in self.chrGroup[chr]:
                    for tsc in tscGroup.transcripts:
                        total_FPKM += float(tsc.attr[expression])
                
            for chr in self.chrGroup:
                for tscGroup in self.chrGroup[chr]:
                    for tsc in tscGroup.transcripts:
                        tsc.attr[tpm_name] = float(tsc.attr[expression])/total_FPKM * MILLION
        return
    
    def combineWith(self, gtf2):
        for chr in set(self.chrGroup.keys()) | set(gtf2.chrGroup.keys()):
            if chr not in self.chrGroup:
                self.chrGroup[chr] = gtf2.chrGroup[chr]
            elif chr in gtf2.chrGroup:
                self.chrGroup[chr].transcripts.extend(gtf2.chrGroup[chr].transcripts)
                self.sort();
        self.tsc_num += gtf2.tsc_num
        self.exon_num += gtf2.exon_num
    
    def getGeneInfo(self, thre = 0):
        gene_info = defaultdict(float)
        for chr in self.chrGroup:
            for tsc in self.chrGroup[chr].transcripts:
                if "gene_name" not in tsc.attr:
                    sys.stderr.write("WARNING: %s transcript %s does not have gene name\n"
                                      % (chr, tsc.attr['transcript_id'] ))
                else:
                    if len(tsc.exons) >= thre:
                        gene_info[tsc.attr['gene_name']] += float(tsc.attr['TPM'])
        return gene_info
    
    def getGeneIsoNum(self, thre = 0):
        isc_num = defaultdict(int)
        for chr in self.chrGroup:
            for tsc in self.chrGroup[chr].transcripts:
                if "gene_name" not in tsc.attr:
                    sys.stderr.write("WARNING: %s transcript %s does not have gene name\n"
                                      % (chr, tsc.attr['transcript_id'] ))
                else:
                    if len(tsc.exons) >= thre:
                        isc_num[tsc.attr['gene_name']] += 1
        return isc_num
    
    def processGTF(self, style=None):
        """ process the SAM file into TSCGroups saved by key chromosome in self.chrGroup   
            1) Each Transcript in chrGroup[chr].transcripts has a dict called attr,
            which contains all the information in annotation string of GTF file, 
            in a key - value pair format
        """
        p_start = 0
        tsc = None
        tsc_hash = {}
        if_sorted = True
        pre_chr = None
        for line in self.fh:
            attribute = {}
            if line[0] == '#': 
                continue
            line = line.strip()
            line = line.strip(';')
            words = re.split(r"\t", line)
            if len(words) < 8:
                R.warning("Illegal content: %s" % line)
                continue
            (chr, category, start, end, anno) = itemgetter(*[0,2,3,4,8])(words)
            no_anno_line = "\t".join(words[0:8])
            start = int(start)
            end = int(end)
            if chr not in self.chrGroup:
                p_start = 0
                self.chrGroup[chr] = R.TSCGroup()
            if start < p_start:
                if if_sorted:
                    warning('The GTF file is not sorted! Isolinks will sort it!')
                    if_sorted = False    
            words = re.split(r"\s*;\s*", anno)
            for each in words:
                key, value = re.split(r'[ =]',each, 1)
                value = value.strip('"')
                attribute[key] = value
            if category == "transcript":
                if tsc is not None:
                    self.chrGroup[pre_chr].transcripts.append(tsc)
                    self.tsc_num += 1
                tsc = R.Transcript(chr, start, end)
                # NOTICE: this attr is a new attribute for Transcript
                tsc.attr = copy.deepcopy(attribute)
                if style=="GENCODE":
                    tsc_hash[tsc.attr["transcript_id"]] = tsc
                elif style=="Pacbio":
                    tsc.read_value = float(tsc.attr['READ_VALUE'])
                    tsc.read_num = float(tsc.attr['READ_VALUE'])
                tsc.status = 'novel'
                tsc.orig = no_anno_line
                pre_chr = chr
                
            elif category == "exon":
                if tsc == None:
                    raise NameError("Any exon should follow a transcript in GTF!")
                exon = R.Exon(chr, start, end)
                exon.orig = line
                if style!="GENCODE":
                    tsc.exons.append(exon)
                else:
                    tsc_hash[attribute['transcript_id']].exons.append(exon)
                self.exon_num += 1
            p_start = start
        # The last transcript needs to processed
        if tsc is not None:
            self.chrGroup[chr].transcripts.append(tsc)
            self.tsc_num += 1
        for chr in self.chrGroup:
            for tsc in self.chrGroup[chr].transcripts:
                tsc.updateExonType()
                tsc.updateExonLen()
        if not if_sorted:
            self.sort()
        self.fh.close()
        
        
        
        