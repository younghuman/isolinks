from __future__ import (absolute_import, division,
                        print_function, unicode_literals)
import ReadGTF as R
import os
import sys
import re
import json
import matplotlib
import itertools
import copy
from time import time
from collections import Counter
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib_venn import venn3
import scipy.stats as stats
import numpy as np
from operator import itemgetter
import ReadGTF as R
import ReadSAM as RSAM

def getRidOfRedundantTSC(GTF):    
    GTF.tsc_num = 0
    GTF.exon_num = 0
    for chr in GTF.chrGroup:
        tscGroups = GTF.chrGroup[chr].findNonOverlappingGroup()
        for tscGroup in tscGroups:
            #sys.stdout.write("[FULL LENGTH]TSC GROUP: %s %d %d\n" % (chr, tscGroup.l_boundary, tscGroup.r_boundary))
            tscGroup.fullLengthTSCFinder()
            GTF.tsc_num += len(tscGroup.anno_transcripts)
            for tsc in tscGroup.anno_transcripts:
                GTF.exon_num += len(tsc.exons)
                tsc.attr = {}
                tsc.status = 'novel'
                tsc.attr['READ_VALUE'] = tsc.read_value
            tscGroup.transcripts = tscGroup.anno_transcripts
        GTF.chrGroup[chr] = tscGroups

def getTSCNum(GTF, thre = 0):
    num = 0
    if isinstance(GTF, R.ReadGTF):
        if thre == 0:
            num = GTF.tsc_num
            return num
        else:
            for chr in GTF.chrGroup:
                if isinstance(GTF.chrGroup[chr], RSAM.TSCGroup):
                    for tsc in GTF.chrGroup[chr].transcripts:
                        if tsc.attr['TPM'] >= thre:
                            num += 1
                elif isinstance(GTF.chrGroup[chr], list):
                    for tscGroup in GTF.chrGroup[chr]:
                        for tsc in tscGroup:
                            if tsc.attr['TPM'] >= thre:
                                num+=1
            return num
    elif isinstance(GTF, list):
        if thre == 0:
            num = len(GTF)
            return num
        else:
            for tsc in GTF:
                if tsc.attr['TPM'] >= thre:
                    num +=1
            return num
    else:
        raise NameError("ERROR: getTSCNum() can only take ReadGTF or list as input!")
                
def getGeneNum(gene_hash, thre = 0):
    num = 0
    for gene in gene_hash:
        if gene_hash[gene] >= thre:
            num += 1
    return num

def combineTwoGeneset(geneset_1, geneset_2):
    combined = copy.deepcopy(geneset_1)
    for gene in geneset_1:
        if gene in geneset_2:
            combined[gene] = min(combined[gene], geneset_2[gene])
        else:
            del combined[gene]
    return combined

def plotVenn3(A,B,C,AB,AC,BC,ABC, labels = ('Isolinks', 'Cufflinks', 'PacBio')
              , thres=[0,2,5], getNum = getTSCNum, level = "Isoform"):
    for thre in thres:
        A_num = getNum(A, thre = thre)
        B_num = getNum(B, thre = thre)
        C_num = getNum(C, thre = thre)
        AB_num = getNum(AB, thre = thre)
        AC_num = getNum(AC, thre = thre)
        BC_num = getNum(BC, thre = thre)
        ABC_num = getNum(ABC, thre = thre)
        Abc = A_num - AB_num - AC_num + ABC_num
        aBc = B_num - AB_num - BC_num + ABC_num
        abC = C_num - AC_num - BC_num + ABC_num
        ABc = AB_num - ABC_num
        AbC = AC_num - ABC_num
        aBC = BC_num - ABC_num
        
        plt.figure(figsize=(8,8))
        v = venn3(subsets = (Abc, aBc, ABc, abC, AbC, aBC, ABC_num),
                 set_labels = labels)
        for text in v.set_labels:
            if text:
                text.set_fontsize(20)
        for text in v.subset_labels:
            if text:
                text.set_fontsize(20)
        title = level + " level overlap (>= %d TPM)"  % thre
        plt.title(title, fontsize=22)
        plt.tight_layout
        plt.savefig("analysis/" + level.lower() + "_venn_%d.png" % thre)
    

if __name__ == "__main__":
    #####################################################
    #         Plot overlap venn at isoform level        #
    #####################################################
    filename = "result/result_20151026_beta_v5/isolinks_transcripts.gtf"
    query = R.ReadGTF(filename)
    query.processGTF()
    query.calculateTPM(expression='READ_VALUE')
    query.summaryStatistics()
    
    filename = "comparison/cufflinks_short/transcripts.gtf"
    query_cuff = R.ReadGTF(filename)
    query_cuff.processGTF()
    query_cuff.calculateTPM(expression = 'FPKM')
    query_cuff.summaryStatistics()
    
    filename = "result/result_20151026_beta_v5/pacbio_transcripts.gtf"
    query_pacbio = R.ReadGTF(filename)
    query_pacbio.processGTF()
    query_pacbio.calculateTPM(expression='READ_VALUE')
    query_pacbio.summaryStatistics() 
    
    cmp_iso_cuff, out_iso_cuff = R.compareGTFTranscripts(query, query_cuff, expression="TPM")
    cmp_iso_pac, out_iso_pac = R.compareGTFTranscripts(query, query_pacbio, expression="TPM")
    cmp_cuff_pac, out_cuff_pac = R.compareGTFTranscripts(query_cuff, query_pacbio, expression="TPM")
    
    out_dir = 'result/result_20151026_beta_v5'
    for key in out_iso_pac:
        if not os.path.isdir(out_dir):
            os.mkdir(out_dir)
        R.transcriptDumpToGTF(out_iso_pac[key], out = os.path.join(out_dir, "iso_pac_" + key + ".gtf"), expression="TPM")
        
    filename = out_dir+"/iso_pac_annotated.gtf"
    query_iso_pac = R.ReadGTF(filename)
    query_iso_pac.processGTF()
    query_iso_pac.summaryStatistics()
    
    cmp_iso_pac_cuff, out_iso_pac_cuff = R.compareGTFTranscripts(query_iso_pac, query_cuff, expression="TPM")
    
    plotVenn3(query, query_cuff, query_pacbio, 
              out_iso_cuff['annotated'], out_iso_pac['annotated'], out_cuff_pac['annotated'],
              out_iso_pac_cuff['annotated'])
    
    #####################################################
    #         Plot overlap venn at gene level           #
    #####################################################
    # Isolinks
    file1 = "result/result_20151026_beta_v5/iso_annotated.gtf"
    query_anno = R.ReadGTF(file1)
    query_anno.processGTF()
    
    file2 = "result/result_20151026_beta_v5/iso_isoforms.gtf"
    query_iso = R.ReadGTF(file2)
    query_iso.processGTF()
    
    query_anno.combineWith(query_iso)
    query_anno.summaryStatistics()
    
    # Cufflinks
    file1 = "result/result_20151026_beta_v5/cuff_annotated.gtf"
    query_cuff_anno = R.ReadGTF(file1)
    query_cuff_anno.processGTF()
    
    file2 = "result/result_20151026_beta_v5/cuff_isoforms.gtf"
    query_cuff_iso = R.ReadGTF(file2)
    query_cuff_iso.processGTF()
    
    query_cuff_anno.combineWith(query_cuff_iso)
    query_cuff_anno.summaryStatistics()
    
    # Pacbio
    file1 = "result/result_20151026_beta_v5/pacbio_annotated.gtf"
    query_pac_anno = R.ReadGTF(file1)
    query_pac_anno.processGTF()
    
    file2 = "result/result_20151026_beta_v5/pacbio_isoforms.gtf"
    query_cuff_iso = R.ReadGTF(file2)
    query_cuff_iso.processGTF()
    
    query_cuff_anno.combineWith(query_cuff_iso)
    query_cuff_anno.summaryStatistics()
    
    geneset_a = query_anno.getGeneInfo()
    geneset_b = query_cuff_anno.getGeneInfo()
    geneset_c = query_pac_anno.getGeneInfo()
    
    geneset_ab = combineTwoGeneset(geneset_a, geneset_b)
    geneset_ac = combineTwoGeneset(geneset_a, geneset_c)
    geneset_bc = combineTwoGeneset(geneset_b, geneset_c)
    
    geneset_abc = combineTwoGeneset(geneset_ab, geneset_c)
    
    plotVenn3(geneset_a, geneset_b , geneset_c,geneset_ab, geneset_ac, geneset_bc, geneset_abc
              , thres=[0,2,5], getNum = getGeneNum, level = "Gene")
    
    
    
    
    
    
    