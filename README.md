# README #

Isolinks is a python written command line tool for third-generation RNA-seq data analysis.

### Dependency ###

* Numpy
* Matplotlib

### Usage ###

You need to download GMAP to first map full-length and non-full-length PacBio read into reference genome.
For example:

`
gmap -D /home/huiyang/project/PacBio/data/hg38_reference/genome -d genome -B 5 -t 8 -n 1 -f samse isoseq_flnc.fasta > flnc.sam
`

Please get rid of the partially mapped alignments with our provided scripts

` 
python sam_map_ratios.py -outsam flnc.good.sam -outimg flnc.png -largerthan 0.9 ../GMAP/fln.sam 
`

Then use SAMTOOLS to sort it

` samtools view -Sb fln.good.sam | samtools sort|samtools view > fln.good.sorted.sam`


Isolinks takes the full-length and non-full-length long read alignment in SAM format as input. 

Then please modify the configuration file config.txt for full length SAM file, non-full-length SAM file, and output path.

Then run Isolinks by:

`python isolinks.py`

NOTICE: the reference transcript annotation should be from GENCODE, which is only useful if you want to identify which transcripts are novel or isoforms.
Please comment that line out if you don't need one.

## Contact of the author ##
Hui Yang            yanghui@usc.edu